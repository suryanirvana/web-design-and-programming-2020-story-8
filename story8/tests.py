from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from .views import *
from selenium import webdriver
import time

# Create your tests here.
class StoryUnitTest (TestCase):
    def test_homepage_url_template_and_function(self):
        response = Client().get('')
        found = resolve('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        self.assertEqual(found.func, index)

class StoryFunctionalTest(LiveServerTestCase):
    def setUp(self) :
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')
    
    def tearDown(self) :
        self.driver.quit()
        super().tearDown()

    def test_functional_for_accordion(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        about_accordion = self.driver.find_element_by_id('about')
        self.assertNotIn('active', about_accordion.get_attribute('class'))

        about_accordion.click()
        self.assertIn('active', about_accordion.get_attribute('class'))

    def test_functional_for_up_and_down(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        up_button = self.driver.find_element_by_class_name('about').find_element_by_class_name('up')
        down_button = self.driver.find_element_by_class_name('about').find_element_by_class_name('down')

        about = response_content.find('About')
        skills = response_content.find('Skills')
        self.assertTrue(about < skills)

        down_button.click()

        response_content = self.driver.page_source

        about = response_content.find('About')
        skills = response_content.find('Skills')
        self.assertTrue(about > skills)

        up_button.click()

        response_content = self.driver.page_source

        about = response_content.find('About')
        skills = response_content.find('Skills')
        self.assertTrue(about < skills)
    
    def test_functional_for_change_theme(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        background = self.driver.find_element_by_tag_name("body").value_of_css_property("background-image")
        text1 = self.driver.find_element_by_id("welcome").find_element_by_class_name("text").value_of_css_property("color")
        text2 = self.driver.find_element_by_id("bottom").value_of_css_property("color")
        accordion = self.driver.find_element_by_id("detail").find_element_by_tag_name("h1").value_of_css_property("background-color")
        accordion_title = self.driver.find_element_by_id("detail").find_element_by_tag_name("h1").value_of_css_property("color")
        accordion_paragraph = self.driver.find_element_by_class_name("more").value_of_css_property("color")

        self.assertEqual("linear-gradient(to left, rgb(238, 242, 243), rgb(142, 158, 171))", background)
        self.assertEqual("rgba(67, 67, 67, 1)",text1)
        self.assertEqual("rgba(238, 242, 243, 1)",text2)
        self.assertEqual("rgba(142, 158, 171, 1)",accordion)
        self.assertEqual("rgba(238, 242, 243, 1)",accordion_title)
        self.assertEqual("rgba(67, 67, 67, 1)",accordion_paragraph)

        self.driver.find_element_by_tag_name("label").click()

        background = self.driver.find_element_by_tag_name("body").value_of_css_property("background-image")
        text1 = self.driver.find_element_by_id("welcome").find_element_by_class_name("text").value_of_css_property("color")
        text2 = self.driver.find_element_by_id("bottom").value_of_css_property("color")
        accordion = self.driver.find_element_by_id("detail").find_element_by_tag_name("h1").value_of_css_property("background-color")
        accordion_title = self.driver.find_element_by_id("detail").find_element_by_tag_name("h1").value_of_css_property("color")
        accordion_paragraph = self.driver.find_element_by_class_name("more").value_of_css_property("color")

        self.assertEqual("linear-gradient(to right, rgb(120, 255, 214), rgb(0, 121, 145))", background)
        self.assertEqual("rgba(238, 242, 243, 1)",text1)
        self.assertEqual("rgba(67, 67, 67, 1)",text2)
        self.assertEqual("rgba(120, 255, 214, 1)",accordion)
        self.assertEqual("rgba(67, 67, 67, 1)",accordion_title)
        self.assertEqual("rgba(238, 242, 243, 1)",accordion_paragraph)
